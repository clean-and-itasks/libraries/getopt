# getopt

This library provides a Clean implementation of getopt, a command line argument parser.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.

Some modules were ported from Haskell and they are provided the compatible
Haskell's 3-clause BSD license (see [LICENSE.BSD3](LICENCE.BSD3)).

- System.GetOpt
